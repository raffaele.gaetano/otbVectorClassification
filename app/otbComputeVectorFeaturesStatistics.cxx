/*=========================================================================
 Program:   ORFEO Toolbox
 Language:  C++
 Date:      $Date$
 Version:   $Revision$


 Copyright (c) Centre National d'Etudes Spatiales. All rights reserved.
 See OTBCopyright.txt for details.


 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbOGRDataSourceWrapper.h"
#include "otbOGRFeatureWrapper.h"
#include "otbStatisticsXMLFileWriter.h"
#include "itkVariableLengthVector.h"

#include <time.h>

namespace otb
{
namespace Wrapper
{
	
/** Utility function to negate std::isalnum */
bool IsNotAlphaNum(char c)
  {
  return !std::isalnum(c);
  }
 
class ComputeVectorFeaturesStatistics : public Application
{
public:
  typedef ComputeVectorFeaturesStatistics Self;
  typedef Application Superclass;
  typedef itk::SmartPointer<Self> Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;
  
  typedef double ValueType;
  typedef itk::VariableLengthVector<ValueType> MeasurementType;
  
  itkNewMacro(Self)
;

  itkTypeMacro(ComputeVectorFeaturesStatistics, otb::Application)
;

private:
  void DoInit() ITK_OVERRIDE
  {
    SetName("ComputeVectorFeaturesStatistics");
    SetDescription("Compute statistics of the features in a set of vector files");

    SetDocLongDescription("Compute statistics (mean and standard deviation) of the features in a set of vector files, and write them in an XML file. This XML file can then be used by the training application.");
    SetDocAuthors("Raffaele Gaetano (CIRAD) based on David Youssefi's implementation (CNES internship)");
    SetDocSeeAlso("VectorClassifier,TrainVectorClassifier");
    AddDocTag(Tags::Segmentation);

    AddParameter(ParameterType_Group, "io", "Input and output data");
    SetParameterDescription("io", "This group of parameters allows setting input and output data.");

    AddParameter(ParameterType_InputVectorDataList, "io.vd", "Input Vector Data List");
    SetParameterDescription("io.vd", "Input vector file list (note : all geometries from the chosen layer will be used)");

    AddParameter(ParameterType_InputFilename, "io.stats", "Output XML image statistics file");
    SetParameterDescription("io.stats", "Output XML file containing mean and variance of each feature.");

    AddParameter(ParameterType_ListView,  "feat", "List of features to consider for statistics.");
    SetParameterDescription("feat","List of features to consider for statistics.");

	AddParameter(ParameterType_Int, "layer", "Layer Index");
    SetParameterDescription("layer", "Index of the layer to use in the input vector files.");
    MandatoryOff("layer");
    SetDefaultParameterInt("layer",0);
	
    // Doc example parameter settings
    SetDocExampleParameterValue("io.vd", "vectorData.shp");
    SetDocExampleParameterValue("io.stats", "results.xml");
    SetDocExampleParameterValue("feat", "perimeter");

  }

  void DoUpdateParameters() ITK_OVERRIDE
  {
    if ( HasValue("io.vd") )
      {
      std::vector<std::string> vectorFileList = GetParameterStringList("io.vd");
      ogr::DataSource::Pointer ogrDS =
        ogr::DataSource::New(vectorFileList[0], ogr::DataSource::Modes::Read);
      ogr::Layer layer = ogrDS->GetLayer(this->GetParameterInt("layer"));
      ogr::Feature feature = layer.ogr().GetNextFeature();

      ClearChoices("feat");
      
      for(int iField=0; iField<feature.ogr().GetFieldCount(); iField++)
        {
        std::string key, item = feature.ogr().GetFieldDefnRef(iField)->GetNameRef();
        key = item;
        std::string::iterator end = std::remove_if(key.begin(),key.end(),IsNotAlphaNum);
        std::transform(key.begin(), end, key.begin(), tolower);
        
        OGRFieldType fieldType = feature.ogr().GetFieldDefnRef(iField)->GetType();
        
        if(fieldType == OFTInteger ||  fieldType == OFTInteger64 || fieldType == OFTReal)
          {
          std::string tmpKey="feat."+key.substr(0, end - key.begin());
          AddChoice(tmpKey,item);
          }
        }
      }
  }

  void DoExecute() ITK_OVERRIDE
  {
      clock_t tic = clock();
	  
      std::vector<MeasurementType> featValue;
	  
	  // Prepare selected field names (their position may change between two inputs)
      std::vector<int> selectedIdx = GetSelectedItems("feat");
      
      if(selectedIdx.empty())
        {
        otbAppLogFATAL(<<"No features have been selected!");
        }
      
      const unsigned int nbFeatures = selectedIdx.size();
      std::vector<std::string> fieldNames = GetChoiceNames("feat");
      std::vector<std::string> selectedNames(nbFeatures);
      for (unsigned int i=0 ; i<nbFeatures ; i++)
        {
        selectedNames[i] = fieldNames[selectedIdx[i]];
        }
      
      std::vector<int> featureFieldIndex(nbFeatures, -1);
	  
	  std::vector<std::string> vectorFileList = GetParameterStringList("io.vd");
	  for (unsigned int k=0 ; k<vectorFileList.size() ; k++)
        {

        otb::ogr::DataSource::Pointer source = otb::ogr::DataSource::New(vectorFileList[k], otb::ogr::DataSource::Modes::Read);
  
		otb::ogr::Layer layer = source->GetLayer(GetParameterInt("layer"));
		otb::ogr::Feature feature = layer.ogr().GetNextFeature();
		
		bool goesOn = feature.addr() != 0;;
		if (!goesOn)
          {
          otbAppLogFATAL("The layer "<<GetParameterInt("layer")<<" of "
            <<vectorFileList[0]<<" is empty!");
          }
		 
		for(unsigned int idx=0; idx < nbFeatures; ++idx) 
		  {
          featureFieldIndex[idx] = feature.ogr().GetFieldIndex(selectedNames[idx].c_str());
            if (featureFieldIndex[idx] < 0)
              otbAppLogFATAL("The field name for feature "<<selectedNames[idx]
              <<" has not been found in the input vector file "<<vectorFileList[0]);
          }

        while(goesOn)
          {
            MeasurementType mv; mv.SetSize(nbFeatures);
            
            for(unsigned int idx=0; idx < nbFeatures; ++idx)
              mv[idx] = feature.ogr().GetFieldAsDouble(featureFieldIndex[idx]);
            
            featValue.push_back(mv);
            feature = layer.ogr().GetNextFeature();
            goesOn = feature.addr() != 0;
          }
		  
        }
  
      MeasurementType mean; mean.SetSize(nbFeatures);
      MeasurementType stddev; stddev.SetSize(nbFeatures);

      for(unsigned int featIt=0; featIt<nbFeatures; featIt++){
       double sum = 0.0; for(unsigned add=0; add<featValue.size(); add++)  sum += featValue[add][featIt];
       mean[featIt] =  sum / featValue.size();
       double accum = 0.0; for(unsigned add=0; add<featValue.size(); add++) accum += (featValue[add][featIt] - mean[featIt]) * (featValue[add][featIt] - mean[featIt]);
       stddev[featIt] = sqrt(accum / (featValue.size()-1)); }
  
      typedef otb::StatisticsXMLFileWriter<MeasurementType> StatisticsWriter;
      StatisticsWriter::Pointer writer = StatisticsWriter::New();
      writer->SetFileName(GetParameterString("io.stats"));
      writer->AddInput("mean", mean);
      writer->AddInput("stddev", stddev);
      writer->Update();

      clock_t toc = clock();
      otbAppLogINFO( "Elapsed: "<< ((double)(toc - tic) / CLOCKS_PER_SEC)<<" seconds.");
  }

};
}
}

OTB_APPLICATION_EXPORT(otb::Wrapper::ComputeVectorFeaturesStatistics)


