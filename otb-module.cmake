set(DOCUMENTATION "OTB Compute Vector Features Statistics.")

otb_module(OTBAppVectorFeaturesStatistics
  DEPENDS
	OTBAppClassification

  DESCRIPTION
    "${DOCUMENTATION}"
)
